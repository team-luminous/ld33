# Be sure to restart your server when you modify this file.
require_dependency 'data_fu/data_includer'

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( game.js )
Rails.application.config.assets.precompile += %w( canvasgame.js )

Sprockets.register_processor 'application/javascript', DataFu::DataIncluder

Rails.application.config.assets.precompile += %w( game.css )
Rails.application.config.assets.precompile += %w( stat.js )
