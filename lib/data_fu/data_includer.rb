module DataFu
  class DataIncluder < Tilt::Template

    DATA_PATTERN = /file_data_uri\(["'](.+?)["']\)/

    def prepare
    end
    def evaluate(scope, locals, &block)
      data.gsub(DATA_PATTERN) do |m|
        make_data_uri($1)
      end
    end

    private
      def make_data_uri(file)
        ext = File.extname(file).at(1..-1)
        mime = Mime::Type.lookup_by_extension ext
        return '"data:'+ mime.to_s + ";base64," + Base64.strict_encode64(IO.binread(Rails.root.join('app', 'assets', file))) + '"'
      end
  end
end 
