class StaticPagesController < ApplicationController
  def home
  end
  def screenres
  end
  def countdown
    script_dir = Rails.root.join('lib', 'countdown.py')
    render text: %x(#{script_dir}), content_type: "image/svg+xml"
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
  def canvas
  end
end
