class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :collect_data, :configure_charsets 
  def collect_data
    Visit.create(user_agent: request.user_agent, ip: request.remote_ip)
  end


  # Configuring charset to UTF-8 
  def configure_charsets 
       headers["Content-Type"] = "text/html; charset=UTF-8"     
  end 

end
