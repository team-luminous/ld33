class AnalyticsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def get_visit_data
    render layout: false, content_type: 'text/plain'
  end
  def save_analytics_data
    data = request.body.read
    hash = ActiveSupport::JSON.decode(data)
    hash['time'] = Time.at(hash['time']).to_datetime
    AnalyticsEvent.create(hash)
    render :nothing => true
  end

  def get_screenres_data
    render layout: false, content_type: 'text/plain'
  end
     
end
