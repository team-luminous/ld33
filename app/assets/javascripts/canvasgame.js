//Here be game code
//For requires, see https://github.com/sstephenson/sprockets#sprockets-directives
//= require cumulativeDensity
//= require helpers.js
//= require tracking.js
//= require prototype.js
//= require stat
//= require pixi
//= require Bob
//= require buzz
//= require cookieType
//= require computer
//= require map
//= require slide
//= require handleMove
//= require_self



// create an new instance of a pixi stage
//var container = new PIXI.Container();
//window.innerWidth/window.innerHeight = 16/9
if (window.innerWidth/16 < window.innerHeight/9){
	var f=window.innerWidth/16;
}
else{
	var f=window.innerHeight/9;
}
var width = f*16;
var height = f*9;
// create a renderer instance.
var renderer = PIXI.CanvasRenderer(width,height);

renderer.backgroundColor = 0x66FF99;

PIXI.loader
  .add('backgroundWithMonitors', file_data_uri('images/taustmonitoridega.png'))
  .add('slidebg', file_data_uri('images/slidetaust.png'))
  .add('a', file_data_uri('images/11.png'))
  .add('b', file_data_uri('images/12.png'))
  .add('c', file_data_uri('images/13.png'))
  .add('d', file_data_uri('images/14.png'))
  .add('bob2', file_data_uri('images/19uus.png'))
  .add('bob1', file_data_uri('images/20uus.png'))
  .add('slidebob', file_data_uri('images/21uus.png'))
  .add('blue', file_data_uri('images/websites/blue.png'))
  .add('s1', file_data_uri('images/websites/1.png'))
  .add('s2', file_data_uri('images/websites/2.png'))
  .add('s3', file_data_uri('images/websites/3.png'))
  .add('s4', file_data_uri('images/websites/4.png'))
  .add('s5', file_data_uri('images/websites/5.png'))
  .add('s6', file_data_uri('images/websites/6.png'))
  .add('s7', file_data_uri('images/websites/7.png'))
  .add('s8', file_data_uri('images/websites/8.png'))
  .add('s9', file_data_uri('images/websites/9.png'))
  .add('s10', file_data_uri('images/websites/10.png'))
  .add('s11', file_data_uri('images/websites/11.png'))
  .add('s12', file_data_uri('images/websites/12.png'))
  .add('s13', file_data_uri('images/websites/13.png'))
  .add('s14', file_data_uri('images/websites/14.png'))
  .add('s15', file_data_uri('images/websites/15.png'))
  .add('instructions',file_data_uri('images/instructions.png'))
  .add('instructions2',file_data_uri('images/happymonster.png'))
  .add('loading',file_data_uri('images/loading.png'))
  .add('tiny', file_data_uri('images/tiny.png'))
  .add('heart', file_data_uri('images/heart.png'))
  .add('end', file_data_uri('images/sadmonster.png'))
  .add('f5button', file_data_uri('images/f5button.png'))
  .add('votebutton', file_data_uri('images/votebutton.png'))
  .add('endscreen', file_data_uri('images/scarymonster.png'))
  .load(startGame); 

//
var stage = new PIXI.Container();

function toGame(e){
  if(e.keyCode==13){
    document.removeEventListener('keydown', toGame);
	var loadingscreen = new PIXI.Sprite(resources.loading.texture);
	loadingscreen.width = width;
	loadingscreen.height = height;
	loadingscreen.position.x = 0;
	loadingscreen.position.y = 0;
    stage.addChild(loadingscreen);
	renderer.render(stage);
    var bob = new Bob();
    var map = new Devmap(width,height,bob);
	stage.visible=false;
  }
}

function toPoster(e){
	if(e.keyCode=13){
		var poster = new PIXI.Sprite(resources.instructions2.texture);
		poster.width = width;
		poster.height = height;
		poster.position.x = 0;
		poster.position.y = 0;
		stage.addChild(poster);
		renderer.render(stage);
		document.removeEventListener('keydown', toPoster);
		document.addEventListener('keydown', toGame);
	}
}

document.addEventListener('keydown', toPoster);

// add the renderer view element to the DOM
function startGame(loader, resources) {
  window.resources = resources;
  document.body.appendChild(renderer.view);
 //set pregame stage
  var pregame = new PIXI.Sprite(resources.instructions.texture);
  pregame.width = width;
  pregame.height = height;
  pregame.position.x = 0;
  pregame.position.y = 0;
  stage.addChild(pregame); 
  renderer.render(stage);
} 

window.onerror = function (message, file, line, col, error) {
    sendError(message + ":" + file + ":" + col);
};

function wrap(func) {
    // Ensure we only wrap the function once.
    if (!func._wrapped) {
        func._wrapped = function () {
            try{
                func.apply(this, arguments);
            } catch(e) {
                // You can send data to your server
                sendError(e.message + ":" + e.stack + ":" + e.lineNumber + ":" + e.columnNumber);
                throw e;
            }
        }
    }
    return func._wrapped;
};

var removeEventListener = window.EventTarget.prototype.removeEventListener;
window.EventTarget.prototype.removeEventListener = function (event, callback, bubble) {
    removeEventListener.call(this, event, callback._wrapped || callback, bubble);
}

function sendError(txt) { Tracking.recordEvent("error", txt); }
