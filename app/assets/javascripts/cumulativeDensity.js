function cumulativeDensity(weights) {
    var sum = weights.sum();
    var rand = Math.floor(Math.random() * sum);
    for (var i = 0; i < weights.length; i++) {
        if(rand < weights[i]) {
            return i;
        } else {
            rand -= weights[i];
        }
    }
}
