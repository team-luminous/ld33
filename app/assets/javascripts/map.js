var speech = document.createElement('div');
var interactive = true;
speech.className = "speech";
speech.style.visibility = "hidden";

var speechContent = document.createElement('div')
speechContent.className = "speechContent"
speech.appendChild(speechContent)
document.body.appendChild(speech);

function setFont() {
    var safePadding = 50; // smaller to be safe
    var maxH = speech.clientHeight
    var fH = 0; // font Height in px
    do {
	speechContent.style.fontSize = fH + "px";
	fH++; // runs once(font+1px) after speechContent height is greater than maxH (font+1px) = font+2px
    } while (speechContent.clientHeight <= maxH && fH < 1000 && speechContent.scrollWidth <= speech.clientWidth);
    // max 1000 because if board empty, then would be infinite loop
    speechContent.style.fontSize = (fH - 2) + "px"; // font-2px = boardC.height <= maxH
}



var Devmap = function(pixiW,pixiH, bob){
    // balancing constants
    this.xcells = 5;
    this.ycells = 3;
    this.animSpeed = 150;
    this.stepCost = 1;
    this.enterCost = 4;

    this.pixiW = pixiW;
    this.pixiH = pixiH;
    this.bob = bob;

    this.container = new PIXI.Container(interactive);
	
    this.bobtext = new PIXI.Text(this.bob.health.toString());
    this.bobtext.y = pixiH-this.bobtext.height-10;
    this.heart = new PIXI.Sprite(resources.heart.texture);
    this.heart.height = this.bobtext.height;
    this.heart.width = this.bobtext.height;
    this.heart.x = pixiW-this.heart.width-10;
    this.heart.y = pixiH-this.heart.height-10;

    this.optOutStatus = false;
    this.firstStep = true;
    var canvas = document.getElementsByTagName('canvas')[0];
    this.canvasX = canvas.offsetLeft;
    this.canvasY = canvas.offsetTop;
    this.optOutStatus = false;
    this.curpos = [0,0];
    this.lastpos = this.curpos.slice(0);
    this.cellSize = [this.pixiW/this.xcells, this.pixiH/this.ycells];
    speech.style.width = this.cellSize[0] + "px";
    speech.style.height = this.cellSize[1] + "px";

    this.jumpSound = new buzz.sound(file_data_uri('sounds/Jump.wav'));
    this.jumpSound.load();
    this.crySound = new buzz.sound(file_data_uri('sounds/Trombone.mp3'));
    this.crySound.load();
	
	this.goodCounter = 0;

    this.screenshots = [
        resources.s1.texture,
        resources.s2.texture,
        resources.s3.texture,
        resources.s4.texture,
        resources.s5.texture,
        resources.s6.texture,
        resources.s7.texture,
        resources.s8.texture,
        resources.s9.texture,
        resources.s10.texture,
        resources.s11.texture,
        resources.s12.texture,
        resources.s13.texture,
        resources.s14.texture,
        resources.s15.texture,
    ];
    this.background = new PIXI.Sprite(resources.backgroundWithMonitors.texture);
    this.background.width = this.pixiW;
    this.background.height = this.pixiH;
    this.background.position.x = 0;
    this.background.position.y = 0;

    this.monster = new PIXI.Sprite(resources.a.texture);
    this.monster.anchor.x = 0;
    this.monster.anchor.y = 0;
    this.monster.width = this.cellSize[0];
    this.monster.height = this.cellSize[1];
    this.monster.position.x = this.curpos[0] * this.cellSize[0];
    this.monster.position.y = this.curpos[1] * this.cellSize[1];
		
	this.replayButton = new PIXI.Sprite(resources.f5button.texture);
	this.replayButton.buttonMode = true;
	this.replayButton.interactive = true;
	this.replayButton.anchor.x = 0;
	this.replayButton.anchor.y = 0;
	this.replayButton.width = this.cellSize[0];
	this.replayButton.height = this.cellSize[1];
	this.replayButton.position.x = this.cellSize[0] * 4;
	this.replayButton.position.y = this.cellSize[1] * 0;
	
	this.replayButton.on('mousedown', this.onButtonDownReplay);
	
	this.voteButton = new PIXI.Sprite(resources.votebutton.texture);
	this.voteButton.buttonMode = true;
	this.voteButton.interactive = true;
	this.voteButton.anchor.x = 0;
	this.voteButton.anchor.y = 0;
	this.voteButton.width = this.cellSize[0];
	this.voteButton.height = this.cellSize[1];
	this.voteButton.position.x = 0;
	this.voteButton.position.y = 0;

	this.voteButton.on('mousedown', this.onButtonDownVote);
		
    this.computers = [];
    for (var x = 0; x < this.xcells; x++) {
        this.computers.push([]);
        for (var y = 0; y < this.ycells; y++) {
            this.computers[x].push(new Computer(this));
            // configure computer screen positions
            this.computers[x][y].screen.width = this.cellSize[1]*300/384;
            this.computers[x][y].screen.height = this.cellSize[0]*150/360;
            if (x == 0) {this.computers[x][y].screen.x = x * this.cellSize[0] + this.cellSize[0]*70/384;};
            if (x == 1) {this.computers[x][y].screen.x = x * this.cellSize[0] + this.cellSize[0]*90/384;}
            if (x == 2) {this.computers[x][y].screen.x = x * this.cellSize[0] + this.cellSize[0]*80/384;}
            if (x == 3) {this.computers[x][y].screen.x = x * this.cellSize[0] + this.cellSize[0]*80/384;}
            if (x == 4) {this.computers[x][y].screen.x = x * this.cellSize[0] + this.cellSize[0]*72/384;}
            if (y == 0) {this.computers[x][y].screen.y = y * this.cellSize[1] + this.cellSize[1]*85/360;} 
            if (y == 1) {this.computers[x][y].screen.y = y * this.cellSize[1] + this.cellSize[1]*50/360;} 
            if (y == 2) {this.computers[x][y].screen.y = y * this.cellSize[1] + this.cellSize[1]*85/360;}
            // configure cookie count text positions
            this.computers[x][y].text.x = x * this.cellSize[0];
            this.computers[x][y].text.y = y * this.cellSize[1];

            this.computers[x][y].tinyCookie.y = this.computers[x][y].text.y;
        }
    }

    this.refreshWorldStatus();
    
    // add stuff to container
    // screens
    for (var x = 0; x < this.xcells; x++) {
        for (var y = 0; y < this.ycells; y++) {
            this.container.addChild(this.computers[x][y].screen); 
        }
    }
    // monitors
    this.container.addChild(this.background);
    // cookie numbers
    for (var x = 0; x < this.xcells; x++) {
        for (var y = 0; y < this.ycells; y++) {
            this.container.addChild(this.computers[x][y].text);  
            this.container.addChild(this.computers[x][y].tinyCookie);
        }
    }
    // bob
    this.container.addChild(this.monster);
    this.container.addChild(this.heart);
    this.container.addChild(this.bobtext);


    this.boundHandleKeypress = this.handleKeypress.bind(this);
    setTimeout(this.fg.bind(this), 1000);
	
}
Devmap.prototype.fg = function(){
    document.body.addEventListener("keyup", this.boundHandleKeypress);
    this.refreshWorldStatus();
    renderer.render(this.container);
    requestAnimationFrame(function(){renderer.render(this.container);}.bind(this))
}
Devmap.prototype.bg = function(){
    document.body.removeEventListener("keyup", this.boundHandleKeypress);
}
Devmap.prototype.handleKeypress = function(e) {
    speech.style.visibility = "hidden";
    this.lastpos = this.curpos.slice(0);
    if (e.which==37) {
        if (this.curpos[0] > 0) this.curpos[0]--;
        this.jumpSound.stop();
        this.jumpSound.play();
        if(this.firstStep == true){
            this.firstStep = false;
            this.mapSpeak("Moving around makes me hungry...There should be cookies inside those computers.", 1, 0);
        }
    } else if (e.which==38) {
        if (this.curpos[1] > 0) this.curpos[1]--;
        this.jumpSound.stop();
        this.jumpSound.play();
        if(this.firstStep == true){
            this.firstStep = false;
            this.mapSpeak("Moving around makes me hungry...There should be cookies inside those computers.", 1, 0);
        }
    } else if (e.which==39) {
        if (this.curpos[0] < this.xcells - 1) this.curpos[0]++;
        this.jumpSound.stop();
        this.jumpSound.play();
        if(this.firstStep == true){
            this.firstStep = false;
            this.mapSpeak("Moving around makes me hungry...There should be cookies inside those computers.", 2, 0);
        }
    } else if (e.which==40) {
        if (this.curpos[1] < this.ycells - 1) this.curpos[1]++;
        this.jumpSound.stop();
        this.jumpSound.play();
        if(this.firstStep == true){
            this.firstStep = false;
            this.mapSpeak("Moving around makes me hungry...There should be cookies inside those computers.", 1, 1);
        }
    } else if (e.which==13 && !this.computers[this.curpos[0]][this.curpos[1]].locked) {
        this.bob.health -= this.enterCost;
        this.bg();
        var slide = new Slide(this.pixiW, this.pixiH, this.bob, this.computers[this.curpos[0]][this.curpos[1]], this)
            return
    } else {
        return
    }
    this.bg();
    var that = this;
    setTimeout(function(){that.step1(that)}, this.animSpeed);
    this.step();
    this.refreshWorldStatus()


        e.preventDefault();
    return false;
}
Devmap.prototype.step = function() {
    this.bob.health -= this.stepCost;
    for (var x = 0; x < this.xcells; x++) {
        for (var y = 0; y < this.ycells; y++) {
            this.computers[x][y].nextStep();
        }
    }
}
Devmap.prototype.refreshWorldStatus = function() {
    // poll bob for health, possibly die

    this.bobtext.text = this.bob.health.toString();
    this.bobtext.x = this.heart.x - this.bobtext.width-2;

    if(this.bob.health < 0){
	this.jumpSound.stop();
        this.bg();
        var losingScreen = new PIXI.Sprite(resources.end.texture);
        losingScreen.height = this.pixiH;
        losingScreen.width = this.pixiW;
        losingScreen.x=0;
        losingScreen.y=0;
        this.container.addChild(losingScreen);
		this.crySound.play();
		console.log("head...", this.goodCounter);
		
		this.goodText = new PIXI.Text("Congratulations, you got " + this.goodCounter.toString() + " cookies!");
		this.goodText.y = this.pixiH/100;
		this.goodText.x = this.pixiW/100;
		this.goodText.height = this.pixiH/4;
		this.goodText.width = this.pixiW/2;
		
		this.sadText = new PIXI.Text("But Bob is still hungry...");
		this.sadText.height = this.pixiH/4;
		this.sadText.width = this.pixiW/2;
		this.sadText.y = this.pixiH - this.pixiH/100 - this.sadText.height;
		this.sadText.x = this.pixiW/100;
		
		this.container.addChild(this.goodText);
		this.container.addChild(this.sadText);
		document.body.addEventListener('keyup', this.endScreen.bind(this));
    }

    // poll computers for their cookie counts
    // update cookie counts
    // update tini cookie x positions
    // update screenshots

    var sortableC = []
        for (var x = 0; x < this.xcells; x++) {
            for (var y = 0; y < this.ycells; y++) {
                this.computers[x][y].text.text = this.computers[x][y].countCookies().toString()+'x';
                this.computers[x][y].tinyCookie.x = this.computers[x][y].text.x+this.computers[x][y].text.width;
                sortableC.push([this.computers[x][y].getGoodPercentage(), x, y]);
            }
        }

    sortableC = sortableC.sort(function(a,b){if (a[0] > b[0]) return -1; if (a[0] < b[0]) return 1; return 0;});
    for (var i = 0; i < 15; i++) {
        if (!this.computers[sortableC[i][1]][sortableC[i][2]].locked) {
	    this.computers[sortableC[i][1]][sortableC[i][2]].screen.texture = this.screenshots[i];
	} else {
	    this.computers[sortableC[i][1]][sortableC[i][2]].screen.texture = resources.blue.texture;
	}
    }
}

Devmap.prototype.mapSpeak = function(wazzup, numberX, numberY){
    var location = this.monster.toGlobal(new PIXI.Point(0, 0));
    var speechX = location['x'] + this.canvasX + this.monster.width*numberX;
    var speechY = location['y'] + this.canvasY + this.monster.height*numberY;
    speechContent.textContent = wazzup;
    speech.style.left = speechX + "px";
    speech.style.top = speechY + "px";
    speech.style.visibility = "visible";
    setFont();
}

Devmap.prototype.step1 = function(that) {
    // only visual
    that.monster.texture = resources.b.texture;
    that.monster.position.x = that.curpos[0] * that.cellSize[0] - (that.curpos[0] - that.lastpos[0]) * that.cellSize[0] / 3 * 2;
    that.monster.position.y = that.curpos[1] * that.cellSize[1] - (that.curpos[1] - that.lastpos[1]) * that.cellSize[1] / 3 * 2;
    renderer.render(this.container);
    setTimeout(function(){that.step2(that)}, that.animSpeed);
}

Devmap.prototype.step2 = function(that) {
    // only visual
    that.monster.texture = resources.c.texture;
    that.monster.position.x = that.curpos[0] * that.cellSize[0] - (that.curpos[0] - that.lastpos[0]) * that.cellSize[0] / 3;
    that.monster.position.y = that.curpos[1] * that.cellSize[1] - (that.curpos[1] - that.lastpos[1]) * that.cellSize[1] / 3;
    renderer.render(this.container);
    setTimeout(function(){that.step3(that)}, that.animSpeed);
}

Devmap.prototype.step3 = function(that) {
    // only visual
    that.monster.texture = resources.d.texture;
    that.monster.position.x = that.curpos[0] * that.cellSize[0];
    that.monster.position.y = that.curpos[1] * that.cellSize[1];
    renderer.render(this.container);
    setTimeout(function(){that.step4(that)}, that.animSpeed);
}

Devmap.prototype.step4 = function(that) {
    // only visual
    that.monster.texture = resources.a.texture;
    this.fg();
}

Devmap.prototype.onButtonDownReplay = function(){
	this.isdown = true;
	location.reload();
}

Devmap.prototype.onButtonDownVote = function(){
	this.isdown = true;
	window.location.href = "http://ludumdare.com/compo/ludum-dare-33/?action=preview&uid=38608";
}

Devmap.prototype.endScreen = function(){
	this.bg();
	this.crySound.stop();
    var endingScreen = new PIXI.Sprite(resources.endscreen.texture);
    endingScreen.height = this.pixiH;
    endingScreen.width = this.pixiW;
    endingScreen.x=0;
    endingScreen.y=0;
    this.container.addChild(endingScreen);
	this.container.addChild(this.replayButton);
	this.container.addChild(this.voteButton);
	renderer.render(this.container);
}
