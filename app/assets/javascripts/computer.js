//= require cookie
//= require hashmap
function Computer(map){
    // balancing variables

    this.stepMin = 1;
    this.stepMax = 2;
    this.initMin = 10;
    this.initMax = 20;
    
    this.map = map;

    this.cookieCount = new HashMap();
    for(var i = 0; i < CookieT.getCookies().length; ++i){
	this.cookieCount.put(CookieT.getCookies()[i], 0);
    }
    
    this.optOutStatus = false;
    this.locked = false;

    var amount = Math.floor(Math.random() * (this.initMax + 1 - this.initMin)) + this.initMin;
    
    this.text = new PIXI.Text(amount.toString());
    this.screen = new PIXI.Sprite(resources.blue.texture);
    this.tinyCookie = new PIXI.Sprite(resources.tiny.texture);
    this.tinyCookie.height = this.text.height;
    this.tinyCookie.width = this.text.height;

    this.createCookies(amount);
	
    this.endSound = new buzz.sound(file_data_uri('sounds/End.mp3'));
    this.endSound.load();
}
Computer.prototype.createCookies = function(amount){
    // getProbabilities will handle allowed/not allowed stuff

    for(var i = 0; i < amount; i++){
        var probabilities = CookieT.getProbabilities(this);
        var cookies = CookieT.getCookies();
        var cookie = cookies[cumulativeDensity(probabilities)];
        if (cookie == optoutCookie) {
            this.setOptOut(true)
                for (c = 0; c < cookies.length; c++) {
                    if (cookies[c].isGood == false) {
                        this.cookieCount.put(cookies[c], 0);
                    }
                }
        }
        this.cookieCount.increment(cookie);
    }
}
Computer.prototype.nextStep = function(){
    console.log("nextStep")
    if(!this.locked){
        var amount = Math.floor(Math.random() * (this.stepMax - this.stepMin)) + this.stepMin;
        this.createCookies(amount);
    }
}
Computer.prototype.setOptOut = function(argument){
    this.map.optOutStatus = argument;
    this.optOutStatus = argument;
}
Computer.prototype.lock = function(){
    this.locked = true;
	console.log("Plahvatab!");
	this.endSound.play();
}
Computer.prototype.countCookies = function(){
    var cookieSum = 0;
    for(var i = 0; i < this.cookieCount.keys().length; ++i){
        cookieSum += this.cookieCount.get(this.cookieCount.keys()[i]);
    }
    return cookieSum;
}
Computer.prototype.getGoodPercentage = function() {
    var total = 0;
    var good = 0;
    for(var i = 0; i < this.cookieCount.keys().length; ++i){
        var c = this.cookieCount.keys()[i];
        total += this.cookieCount.get(c);
	if (typeof c == "undefined") debugger;
	if(c.isGood) {
	    good += this.cookieCount.get(c);
	}
    }
    return good/total;
}
