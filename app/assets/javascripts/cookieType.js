var CookieT = (function() {
    var allCookies = [];
    var nextID = 0;
    var that = function(isGood, crunch, probability, texture, seen, text) {
        this.isGood = isGood;
        this.crunch = crunch;
        this.probability = probability;
        this.texture = texture;
        this.seen = seen;
        this.text = text;
        var id = nextID;
        nextID++;
        allCookies.push(this);
        this.toString = function(){ return "Cookie" + id; };
    };
    that.getCookies = function(){return allCookies;};
    that.getProbabilities = function(computer) {
        var resp = [];
        //  console.log(computer);
        for (var i = 0; i < allCookies.length; i++) {
            if ((computer.optOutStatus && !allCookies[i].isGood) || (computer.map.optOutStatus && allCookies[i] == optoutCookie)) {
                resp.push(0);
            } else {
                resp.push(allCookies[i].probability);
            }
        }
        return resp;
    }
    return that;
})();

var cartCookie = new CookieT(true,  function(bob, computer, slide){bob.health += 1; computer.cookieCount.decrement(this);slide.bigStuffText.text='+1';slide.bigStuffText.style.fill='#22FF22';slide.container.addChild(slide.bigStuffText);slide.firstScoreTimeFirst = true;slide.score(performance.now());slide.showScore=true;}, 5, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/shoppingcart.png')), true, "shoppingcart");
var settingsCookie = new CookieT(true,  function(bob, computer, slide){bob.health += 1; computer.cookieCount.decrement(this);slide.bigStuffText.text='+1';slide.bigStuffText.style.fill='#22FF22';slide.container.addChild(slide.bigStuffText);slide.firstScoreTimeFirst = true;slide.score(performance.now());slide.showScore=true;}, 7, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/settings.png')), true, "settings");
var badLoginCookie = new CookieT(true,  function(bob, computer, slide){bob.health += 5; computer.cookieCount.decrement(this);slide.bigStuffText.text='+5';slide.bigStuffText.style.fill='#22FF22';slide.container.addChild(slide.bigStuffText);slide.firstScoreTimeFirst = true;slide.score(performance.now());slide.showScore=true;}, 1, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/badloginuus.png')), false, "This cookie spilled some riches! Now I can buy a whole box worth!");
var trackingCookie = new CookieT(false, function(bob, computer,slide){bob.health -= 1; computer.cookieCount.decrement(this);slide.bigStuffText.text='-1';slide.bigStuffText.style.fill='#FF2222';slide.container.addChild(slide.bigStuffText);slide.firstScoreTimeFirst = true;slide.score(performance.now());slide.showScore=true;}, 5, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/tracker.png')), true, "tracker");
var GSC = new CookieT(false, function(bob, computer, slide){computer.cookieCount.put(this, 0); computer.lock()}, 2, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/notOreo.png')), false, "Oups, I should not have eaten that...");
var flashCookie = new CookieT(false, function(bob, computer, slide){bob.health -= 1;slide.bigStuffText.text='-1';slide.bigStuffText.style.fill='#FF2222';slide.container.addChild(slide.bigStuffText);slide.firstScoreTimeFirst = true;slide.score(performance.now());slide.showScore=true;}, 10, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/flash.png')), false, "YUCK! I'll just leave this here...and now I am even hungrier...");
var HTTPCookie = new CookieT(false, function(bob, computer, slide){}, 2, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/HTTPS.png')), false, "Hey, no fair! The counter promised more cookies...");
var optoutCookie = new CookieT(true,  function(bob, computer, slide){bob.health += 3; computer.cookieCount.decrement(this); computer.setOptOut(false);slide.bigStuffText.text='+3';slide.bigStuffText.style.fill='#22FF22';slide.container.addChild(slide.bigStuffText);slide.firstScoreTimeFirst = true;slide.score(performance.now());slide.showScore=true;}, 1, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/chocolatechip.png')), false, "So big! So tasty!");
var HTTPSCookie = new CookieT(true,  
        function(bob, computer, slide){
            var probabilities = CookieT.getProbabilities(computer).slice(0, -2); //Exclude Opt-out and HTTPS
            var index = cumulativeDensity(probabilities);
            var t = CookieT.getCookies()[index];
            computer.cookieCount.increment(t);
            t.crunch(bob, computer, slide);
            computer.cookieCount.decrement(this)
        }, 6, PIXI.Texture.fromImage(file_data_uri('images/prettycookies/HTTPS.png')), false, "A surprise cookie! Better be careful with those!");



