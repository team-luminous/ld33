var HTTPFirst = true;

var Slide = function(pixiW, pixiH, bob, computer, map){
    this.firstScoreTimeFirst = false;
    this.speed = 0.3; // screens/s
    this.columnSpacing = computer.cookieCount.sumValues();
    if (this.columnSpacing > (pixiW / (pixiH / 7)) / 4) this.columnSpacing = (pixiW / (pixiH / 7)) / 4 // hu
    if (this.columnSpacing < 1) this.columnSpacing = 1;


    var gameTime = 100;
    var columnSparseness = -3 / (this.columnSpacing + 3) + 1;

    this.cookieSpeed = (computer.cookieCount.sumValues()/ ( 5 * (1-columnSparseness) * this.columnSpacing * (pixiH / 7) / pixiW )/gameTime);

    this.speed = computer.cookieCount.sumValues() * 0.02; // screens/s

    this.map = map;
    this.bob = bob;
    this.bobtext = new PIXI.Text(this.bob.health.toString());
    this.bobtext.y = pixiH-this.bobtext.height-10;
    this.heart = new PIXI.Sprite(resources.heart.texture);
    this.heart.height = this.bobtext.height;
    this.heart.width = this.bobtext.height;
    this.heart.x = pixiW-this.heart.width-10;
    this.heart.y = pixiH-this.heart.height-10;


    this.computer = computer;
    this.pixiH = pixiH;
    this.pixiW = pixiW;

    var canvas = document.getElementsByTagName('canvas')[0];
    this.canvasX = canvas.offsetLeft;
    this.canvasY = canvas.offsetTop;

    this.container = new PIXI.Container();
    this.onUpdate = [];
    this.lateUpdate = [];
    this.paused = false;

    this.fpsCounter = new PIXI.Text("NaN FPS"/*, {font : '14px Arial', fill : 0xff0000, align : 'left'}*/);
    this.fpsCounter.position.x = 50;
    this.fpsCounter.position.y = 50;
    this.container.addChild(this.fpsCounter);

    this.boundHandleKeyDown = this.handleKeyDown.bind(this);
    this.boundHandleKeyUp = this.handleKeyUp.bind(this);
    document.body.addEventListener("keydown", this.boundHandleKeyDown);
    document.body.addEventListener("keyup", this.boundHandleKeyUp);

    var background = new PIXI.Sprite(resources.slidebg.texture);
    background.width = pixiW;
    background.height = pixiH;
    background.position.x = 0;
    background.position.y = 0;      

    this.monster = new PIXI.Sprite(resources.slidebob.texture);
    this.container.addChild(background);
    this.container.addChild(this.monster);
    this.container.addChild(this.heart);
    this.container.addChild(this.bobtext);
    this.monster.anchor.x = 0;
    this.monster.anchor.y = 0.5;
    this.monster.position.x = 20;
    this.monster.position.y = pixiH / 2;
    var prevh = this.monster.height;
    this.monster.height = this.pixiH / 7 * 2;
    this.monster.width = this.monster.width * this.monster.height / prevh;
	
	var monsterEat = [resources.slidebob.texture, resources.bob1.texture, resources.bob2.texture];
	this.eatCookie = new PIXI.extras.MovieClip(monsterEat);
	this.eatCookie.anchor.x = this.monster.anchor.x;
	this.eatCookie.anchor.y = this.monster.anchor.y;
	this.eatCookie.height = this.monster.height;
	this.eatCookie.width = this.monster.width;
	this.eatCookie.animationSpeed = 0.25;
	this.eatCookie.loop = false;
	
    this.lastTime = performance.now();

    this.animFrame = window.requestAnimationFrame(this.update.bind(this));

    if(this.computer.cookieCount.get(HTTPCookie) > 0 && !HTTPCookie.seen){
        HTTPCookie.seen = true;
        this.bobtext.x = this.heart.x-this.bobtext.width-2;
        this.slideSpeak(HTTPCookie.text);
        renderer.render(this.container);
        this.pause();
    }
    //.keys.put.set.sumValues
    this.path = [];
    var cookieCount = computer.cookieCount.deepCopy();

    if(computer.optOutStatus) {
        cookieCount.decrement(optoutCookie);
        this.path.push([null,null,null,null,null]);
        optOutArray = [];
        var rand = Math.floor(Math.random() * 3);
        for(var i = 0; i < rand; ++i) optOutArray.push(null);
        optOutArray.push(optoutCookie);
        for(var i = 0; i < 4 - rand; ++i) optOutArray.push(null);
        this.path.push(optOutArray);
        this.path.push([null,null,null,null,null]);
        this.path.push([null,null,null,null,null]);
    }
    while (cookieCount.sumValues() > 0) {
        var column = [];
        for (var c = 0; c < 5; c++) {
            if(cookieCount.sumValues() <= 0) { 
                while(c < 5) {
                    ++c;
                    column.push(null);
                }
                break;
            }
            if (Math.random() < columnSparseness) {
                column.push(null);
                continue;
            }
            var amounts = [];
            for (var i = 0; i < cookieCount.keys().length; i++) {
                amounts.push(cookieCount.get(cookieCount.keys()[i]));
            }
            var index = cumulativeDensity(amounts);
            var cookie = cookieCount.keys()[index];
            column.push(cookie);
            cookieCount.decrement(cookie);
        }
        this.path.push(column)
    }

    this.cookieJar = new PIXI.Container();
    this.cookieJar.position.x = pixiW / 2;


    this.container.addChild(this.cookieJar);
    var o = false;
    var g = new PIXI.Graphics();
    g.drawCircle(0,0,1);
    this.cookieJar.addChild(g);

    for(var i = 0; i < this.path.length; ++i) {
        var column = this.path[i];
        for(var j = 0; j < 5; ++j) {
            var c = column[j];
            if (c==null) continue;
            if(c == undefined) debugger;
            var cookieSprite = new PIXI.Sprite(c.texture);
            cookieSprite.position.y = ( j + 1) * (this.pixiH/7);
            cookieSprite.position.x =  i * (pixiH / 7 * this.columnSpacing);
            var prevh = cookieSprite.height;
            cookieSprite.height = this.pixiH / 7.4;
            cookieSprite.width = cookieSprite.width * cookieSprite.height / prevh;
            if(c == optoutCookie) {
                cookieSprite.height *= 3;
                o = true;
                cookieSprite.width *= 3;
                cookieSprite.anchor.y = 1 / 3;
                cookieSprite.position.y += this.pixiH/7;
                cookieSprite.positions = [];
                for(var k = 0; k < 3; ++k) {
                    var fakie = {};
                    fakie.x = cookieSprite.position.x + pixiH / 7 * this.columnSpacing;
                    fakie.y = cookieSprite.position.y + (k - 1) * (this.pixiH/7);
                    cookieSprite.positions.push(fakie);
                }
            }
            cookieSprite.cookieT = c;
            this.cookieJar.addChild(cookieSprite);
            console.log(cookieSprite.position.x, cookieSprite.position.y);
        }
    }
    var graphics = new PIXI.Graphics();
    graphics.beginFill(0xff0000, 1);
    graphics.lineWidth = 0;
    graphics.moveTo(0, this.cookieJar.y);
    graphics.lineTo(0, this.cookieJar.height);
    graphics.moveTo(this.cookieJar.width + o * 2 * this.columnSpacing, this.cookieJar.y);
    graphics.lineTo(this.cookieJar.width + o * 2 * this.columnSpacing, this.cookieJar.height);
    graphics.endFill();
    this.cookieJar.addChild(graphics);
    window.co = this.container;
    this.eatSound = new buzz.sound(file_data_uri('sounds/Chip.mp3'));
    this.eatSound.load();
 

    this.bigStuffText = new PIXI.Text('Ready!');
    this.bigStuffText.style.fill='#99DDFF';
    this.bigStuffText.anchor.set(0.5);
    this.bigStuffText.x = pixiW/2;
    this.bigStuffText.y = pixiH/2;
    this.bigStuffText.alpha = 1;
    this.number = 2;
    this.isCountDownFinished = false;
    this.firstBigStuffTime = performance.now();
    this.bigStuffHeight = this.bigStuffText.height;
    this.bigStuffWidth = this.bigStuffText.width;
    this.container.addChild(this.bigStuffText);
    
    this.firstScoreTime=0;
    this.showScore = false;
    
//    thia.    

}


Slide.prototype.moveCookies = function(dt){
    var lastJarX = this.cookieJar.position.x;
    this.cookieJar.position.x -= 1 * dt/1000 * this.cookieSpeed * this.pixiW;
    if (this.cookieJar.position.x < -this.cookieJar.width - this.pixiW / 7) {
        this.stop();
        this.map.fg();
    }
    var cookies = this.cookieJar.children;
    for(var i = cookies.length - 1; i >= 0; --i) {
        var cookie = cookies[i];
        if(typeof cookie.cookieT == "undefined") continue;
        var pos = cookie.position;
        //Parallelogram (x1;y1);(x2;y2);(x3;y3);(x4;y4)
        //Cookie point (x5;y5)

        function detectCollision(x1, x2, x3, x4, x5, y1, y2, y3, y4, y5) {
            function lineFunction(ax, ay, bx, by) {
                function genericLine(sl, y0, x) {
                    return y0 + sl * x;
                }
                var slope = (by - ay) / (bx - ax);
                var y0 = ay - slope * ax;
                return genericLine.bind(this, slope, y0);
            }
            return x5 > x1 && x5 < x3 && y5 > lineFunction(x1, y1, x3, y3)(x5) && y5 < lineFunction(x2, y2, x4, y4)(x5);
        }
        var x1, x2, x3, x4, x5, y1, y2, y3, y4, y5;
        x1 = x2 = -lastJarX + this.monster.position.x + 3 / 5 * this.monster.width; 
        x3 = x4 = -this.cookieJar.position.x + this.monster.position.x + 3 / 5 * this.monster.width;
        x5 = pos.x;
        y1 = this.monster.lastPosition.y - 2 / 3 * this.monster.height;
        y2 = this.monster.lastPosition.y + 1 / 7 * this.monster.height;
        y3 = this.monster.position.y - 2 / 3 * this.monster.height;
        y4 = this.monster.position.y + 1 / 7 * this.monster.height;
        y5 = pos.y;
        //console.log(x1, x2, x3, x4, x5, y1, y2, y3, y4, y5);
        //console.log(x5 > x1);
        //console.log(x5 < x3);
        var col = detectCollision(x1, x2, x3, x4, x5, y1, y2, y3, y4, y5);
        if(typeof cookie.positions != "undefined") {
            for(var o = 0; o < cookie.positions.length; ++o) {
                x1 = x2 = -lastJarX + this.monster.position.x + 3 / 5 * this.monster.width; 
                x3 = x4 = -this.cookieJar.position.x + this.monster.position.x + 3 / 5 * this.monster.width;
                x5 = cookie.positions[o].x;
                y1 = this.monster.lastPosition.y - 2 / 3 * this.monster.height;
                y2 = this.monster.lastPosition.y + 1 / 7 * this.monster.height;
                y3 = this.monster.position.y - 7 / 9 * this.monster.height;
                y4 = this.monster.position.y + 1 / 7 * this.monster.height;
                y5 = cookie.positions[o].y;
                /*                    var graphics = new PIXI.Graphics();
                                      graphics.beginFill(0xff0000);
                                      graphics.drawCircle(x5, y5, 2);
                                      graphics.endFill();
                                      this.cookieJar.addChild(graphics);*/
                col |= detectCollision(x1,x2,x3,x4,x5,y1,y2,y3,y4,y5);
            }
        }

        if(col) {
            this.eatCookie.position.x = this.monster.position.x;
            this.eatCookie.position.y = this.monster.position.y;
            this.eatCookie.play();
            this.eatCookie.onComplete = function(){
                this.container.removeChild(this.eatCookie);
                this.monster.visible = true;
            }.bind(this);
            
            this.monster.visible = false;
            this.container.addChild(this.eatCookie);
			if(cookie.cookieT.isGood == true){
				this.map.goodCounter++;
			}
            cookie.cookieT.crunch(this.bob,this.computer,this);
            this.eatSound.play();
            if(cookie.cookieT.seen == false){
                this.pause();
                cookie.cookieT.seen = true;
                this.slideSpeak(cookie.cookieT.text);
            }
            if(cookie.cookieT == GSC) {
                this.stop();
                this.map.fg();
				this.map.mapSpeak("Error, you have broken the computer.", 0, 0);
            }
            if(cookie.cookieT != flashCookie) {
                this.cookieJar.removeChild(cookie);
            }
        }
    }
}

Slide.prototype.pause = function() {
    this.paused = true;
    cancelAnimationFrame(this.animFrame);
}
Slide.prototype.play = function() {
    this.paused = false;
    this.animframe = window.requestAnimationFrame(this.update.bind(this));
    this.lastTime = performance.now();
    if (!this.isCountDownFinished) this.firstBigStuffTime = performance.now();
}
Slide.prototype.stop = function() {
    this.pause();
    document.body.removeEventListener("keydown", this.boundHandleKeyDown);
    document.body.removeEventListener("keyup", this.boundHandleKeyDown);
}

Slide.prototype.update = function(timestamp) {
    console.log(this.cookieJar.width);
    this.bigStuff(timestamp);
    if(this.showScore){
        this.score(timestamp);
    }
    if (this.paused) return
        var dt = timestamp - this.lastTime;
    this.lastTime = timestamp;
    this.monster.lastPosition = this.monster.position;
    for(var i = 0; i < this.onUpdate.length; ++i) {
        this.onUpdate[i].bind(this)(dt);        
    }
    for(var i = 0; i < this.lateUpdate.length; ++i) {
        this.lateUpdate[i].bind(this)(dt);        
    }
    this.bobtext.text = this.bob.health.toString();
    this.bobtext.x = this.heart.x-this.bobtext.width-2;

    this.fpsCounter.text = (1 / dt * 1000) + " FPS";

    this.render.bind(this)();
    this.animFrame = window.requestAnimationFrame(this.update.bind(this));

}

Slide.prototype.render = function(){
    renderer.render(this.container);
}
Slide.prototype.up = function(dt){
    if (this.monster.position.y - this.monster.height/2 + 20 > 0) this.monster.position.y -= dt/1000*this.speed*this.pixiH;
}
Slide.prototype.down = function(dt){
    if (this.monster.position.y - 20 < this.pixiH-this.monster.height/2) this.monster.position.y += dt/1000*this.speed*this.pixiH;
}
Slide.prototype.handleKeyDown = function(e) {
    if ((e.which==27||e.which==13) && this.paused) {
        speech.style.visibility = "hidden";
        this.play()
    } else if (e.which==27) {
        this.pause()
    }
    
    if (e.which==38 && this.onUpdate.indexOf(this.up) == -1) {
        this.onUpdate.push(this.up);
    } else if (e.which==40 && this.onUpdate.indexOf(this.down) == -1) {
        this.onUpdate.push(this.down);
    }
}

Slide.prototype.handleKeyUp = function(e) {
    if (e.which==38) {
        index = this.onUpdate.indexOf(this.up);
        this.onUpdate.splice(index, 1);
    } else if (e.which==40) {
        index = this.onUpdate.indexOf(this.down);
        this.onUpdate.splice(index, 1);
    }
}

Slide.prototype.slideSpeak = function(wazzup){
    var location = this.monster.toGlobal(new PIXI.Point(0, 0));
    var speechX = location['x'] + this.map.canvasX + this.monster.width;
    var speechY = location['y'] + this.map.canvasY;
    speechContent.textContent = wazzup;
    speech.style.left = speechX + "px";
    speech.style.top = speechY + "px";
    speech.style.visibility = "visible";
    setFont();
}

Slide.prototype.bigStuff = function(timestamp){
    if (this.paused) return // don't run while paused
    if((timestamp-this.firstBigStuffTime)/1000>2){
        this.bigStuff = function(){};
        this.container.removeChild(this.bigStuffText);
        return;
    }
    if(Math.floor(2-(timestamp-this.firstBigStuffTime)/1000)+1<this.number){
        this.number = Math.floor(2-(timestamp-this.firstBigStuffTime)/1000)+1;
        this.bigStuffText.height = this.bigStuffHeight;
        this.bigStuffText.width = this.bigStuffWidth;
        this.bigStuffText.alpha = 1;
    }
    if (this.number ==1 && this.lateUpdate.indexOf(this.moveCookies) == -1) {
        this.lateUpdate.push(this.moveCookies);
    }
    this.bigStuffText.height *= (1+((3-this.number)-(timestamp-this.firstBigStuffTime)/1000)/10);
    this.bigStuffText.width *= (1+((3-this.number)-(timestamp-this.firstBigStuffTime)/1000)/10);
    this.bigStuffText.alpha -= 0.015;
    if (this.number == 2) this.bigStuffText.text = "Ready!";
    if (this.number == 1) this.bigStuffText.text = "Go!";
}

Slide.prototype.score = function(timestamp){
    if(this.firstScoreTimeFirst){
        this.firstScoreTimeFirst = false;
        this.firstScoreTime = timestamp;
        this.bigStuffText.height = this.bigStuffHeight;
        this.bigStuffText.width = this.bigStuffWidth;
        this.bigStuffText.alpha = 1;
    }
    this.bigStuffText.alpha -= 0.02;
    this.bigStuffText.height *= (1+(1-(timestamp-this.firstScoreTime)/1000)/8);
    this.bigStuffText.width *= (1+(1-(timestamp-this.firstScoreTime)/1000)/8);
    if((timestamp-this.firstScoreTime)/1000>1){
        this.showScore=false;
        this.bigStuffText.alpha = 0;
    }
}
