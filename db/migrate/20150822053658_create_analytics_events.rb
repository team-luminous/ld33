class CreateAnalyticsEvents < ActiveRecord::Migration
  def change
    create_table :analytics_events do |t|
      t.string :key
      t.string :value
      t.string :clientID
      t.string :sessionID
      t.datetime :time

      t.timestamps null: false
    end
  end
end
