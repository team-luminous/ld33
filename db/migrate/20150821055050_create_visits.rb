class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.string :ip
      t.string :user_agent

      t.timestamps null: false
    end
  end
end
