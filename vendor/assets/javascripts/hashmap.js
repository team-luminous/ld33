function HashMap(){
    this._dict = [];
}
HashMap.prototype._get = function(key){
    for(var i=0, couplet; couplet = this._dict[i]; i++){
        if(couplet[0] === key){
            return couplet;
        }
    }
}
HashMap.prototype.put = function(key, value){
    if(typeof key == "undefined") debugger;
    var couplet = this._get(key);
    if(couplet){
        couplet[1] = value;
    }else{
        this._dict.push([key, value]);
    }
    return this; // for chaining
}
HashMap.prototype.get = function(key){
    var couplet = this._get(key);
    if(couplet){
        return couplet[1];
    }
}

HashMap.prototype.keys = function() {
    var keys = [];
    for(var i = 0; i < this._dict.length; ++i) {
        keys.push(this._dict[i][0]);
    }
    return keys;
}


HashMap.prototype.sumValues = function(){
    var sum = 0;
    for(var i = 0; i < this._dict.length; ++i) {
        sum += this._dict[i][1];
    }
    return sum;
}

HashMap.prototype.deepCopy = function() {
    var dc = new HashMap();
    for(var i = 0; i < this._dict.length; ++i)
    {
        dc._dict.push([this._dict[i][0], this._dict[i][1]]);
    }
    return dc;
}

HashMap.prototype.decrement = function(key) {
    this.put(key, this.get(key)-1);
    if(this.get(key) < 0) debugger;
}

HashMap.prototype.increment = function(key) {
    this.put(key, this.get(key)+1);
}
