//=require cookie.js
"use strict"
var Tracking = function(){
	var address = "/stats";
	function guid() {
  		function s4() {
    			return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	function generateID()
	{
		return guid();	
	}
	var getSessionID = generateID;
	function getClientID()
	{
		var id = Cookie.get("cid");
		if(id == "")
		{
			id = generateID();
			Cookie.set("cid", id, 7000);
		}
		return id;
	}
	var clientID = getClientID();
	var sessionID = getSessionID();
	function constructMessage(key, value)
	{
		return {
			time: new Date().getTime(),
			key: key,
			value: value,
			"clientID": clientID,
			"sessionID": sessionID,
		}
	}
	function pack(msg){
		var json = JSON.stringify(msg);
		return json;
	}
	return {
		recordEvent: function(eventName, value) {
			var msg = constructMessage(eventName, value);
			var data = pack(msg);
			var xmlhttp = new XMLHttpRequest();
			xmlhttp.open("POST",address,true);
			xmlhttp.send(data);
		}
	};
}()
